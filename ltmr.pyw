# by loRe
from datetime import UTC
from datetime import datetime as dt

from enum import IntEnum
from tkinter import font, Button, StringVar, Tk, Entry, Frame, IntVar, END

from playsound import playsound

DEF_WORK_TIME = 20
DEF_EXT = 5
DEF_PERIOD = 60 * 1000
# DEF_PERIOD = 60 * 10

LOG_FILE = str(dt.now().date()) + '.csv'
SOUND_FILE = 'noti.wav'


class ST(IntEnum):
    WAITING = 0
    TIMER = 1
    PAUSING = 2
    EXTEND = 3
    EXT_TIMER = 4


root = Tk()
root.title('NotifyMe')
# root['bg'] = 'black'
root.option_add("*Button.Background", "black")
root.option_add("*Button.Foreground", "gray")

entry_font = font.nametofont("TkDefaultFont").copy()
entry_font.config(size=24)

font.nametofont("TkDefaultFont").config(size=14)
root.resizable(0, 0)
# root.attributes('-topmost', True)
# root.overrideredirect(True)
root.timer_state = ST.WAITING
root.last_timer_state = ST.WAITING
root.remainder = 0
root.start_time = None

txt_title = StringVar(value='misc')
txt_timer = IntVar(value=DEF_WORK_TIME)

ent_title = Entry(root, textvariable=txt_title, bg='black', fg='gray', disabledbackground='black',
                  insertbackground='white', font=entry_font,
                  justify='center')
ent_title.grid(row=0, column=0, sticky='we')
ent_title.bind('<FocusIn>', lambda x: ent_title.selection_range(0, END))
ent_title.focus()
ent_timer = Entry(root, textvariable=txt_timer, bg='black', fg='gray', disabledbackground='black',
                  insertbackground='white', font=entry_font,
                  justify='center')
ent_timer.grid(row=1, column=0, sticky='we')
ent_timer.bind('<FocusIn>', lambda x: ent_timer.selection_range(0, END))
ent_title.bind('<Return>', lambda x: ent_timer.focus())


def release_title():
    ent_title.config(state='normal')


def release_timer():
    ent_timer.config(state='normal')


def freeze_title():
    ent_title.config(state='disabled')


def freeze_timer():
    ent_timer.config(state='disabled')


def add_button(frame, text, command, column, row=0, span=1):
    Button(frame, text=text, command=command).grid(row=row, column=column, columnspan=span, sticky='we')
    frame.grid_columnconfigure(column, weight=1)


def log_start():
    root.start_time = int(dt.now(UTC).timestamp())
    root.start_time_str = str(dt.now().ctime())


def log_stop():
    if root.start_time:
        stop_time = int(dt.now(UTC).timestamp())
        stop_time_str = str(dt.now().ctime())

        with open(LOG_FILE, 'a') as f:
            f.write(
                '%d, %d, "%s", "%s", "%s"\n' % (root.start_time, stop_time, root.start_time_str, stop_time_str,
                                                txt_title.get()))

        root.start_time = None


def btncb_start(time):
    log_start()
    root.remainder = time
    switch_state(ST.TIMER)


def btncb_stop():
    log_stop()
    root.remainder = 0
    switch_state(ST.WAITING)


def btncb_pause():
    log_stop()
    switch_state(ST.PAUSING)


def btncb_resume():
    log_start()
    root.remainder = txt_timer.get()
    switch_state(root.last_timer_state)


def btncb_extend(time):
    root.remainder = time
    switch_state(ST.EXT_TIMER)


# State 0: waiting for beginning
# <start> <start 30>
frm_start = Frame(root)
add_button(frm_start, text='Start', command=lambda: btncb_start(txt_timer.get()), row=0, column=0)
add_button(frm_start, text='Start 30m', command=lambda: btncb_start(30), row=0, column=1)
frm_start.ori_raise = frm_start.tkraise


def r():
    txt_timer.set(DEF_WORK_TIME)
    ent_timer.bind('<Return>', lambda x: btncb_start(txt_timer.get()))
    release_title()
    release_timer()
    frm_start.ori_raise()


frm_start.tkraise = r

# State 1: working
# <stop> <pause>
frm_timer = Frame(root)
add_button(frm_timer, text='Stop', command=btncb_stop, column=0)
add_button(frm_timer, text='Pause', command=btncb_pause, column=1)
frm_timer.ori_raise = frm_timer.tkraise


def r():
    freeze_title()
    freeze_timer()
    ent_timer.bind('<Return>', None)
    frm_timer.ori_raise()


frm_timer.tkraise = r

# State 2: pause
# <stop> <Resume>
frm_pause = Frame(root)
add_button(frm_pause, text='Stop', command=btncb_stop, column=0)
add_button(frm_pause, text='Resume', command=btncb_resume, column=1)
frm_pause.ori_raise = frm_pause.tkraise


def r():
    freeze_title()
    release_timer()
    ent_timer.bind('<Return>', lambda x: btncb_resume())
    ent_timer.focus()
    frm_pause.ori_raise()


frm_pause.tkraise = r

# State 3: end
# (alarm)
# <extend> <extend 10> <stop>
frm_extend = Frame(root)
add_button(frm_extend, text='Extend', command=lambda: btncb_extend(txt_timer.get()), column=0)
add_button(frm_extend, text='Extend 10m', command=lambda: btncb_extend(10), column=1)
add_button(frm_extend, text='Stop', command=btncb_stop, column=2)
frm_extend.ori_raise = frm_extend.tkraise


def r():
    freeze_title()
    txt_timer.set(5)
    release_timer()
    ent_timer.bind('<Return>', lambda x: btncb_extend(txt_timer.get()))
    ent_timer.focus()
    frm_extend.ori_raise()


frm_extend.tkraise = r

# State 4: extended timer
# same as Status 1
frm_extend_timer = frm_timer

frames = (frm_start, frm_timer, frm_pause, frm_extend, frm_extend_timer)

for f in frames:
    f['bg'] = 'black'
    f.grid(row=2, column=0, sticky='news')


def switch_state(state_num):
    root.last_timer_state = root.timer_state
    root.timer_state = state_num
    frames[state_num].tkraise()
    if root.timer_state == ST.WAITING:
        pass
    elif root.timer_state == ST.PAUSING:
        pass
    elif root.timer_state == ST.EXTEND:
        pass
    else:
        pass


def key_callback(event):
    if event.char == '\r':
        pass
    elif event.char == '\x1b':
        pass


def timer_callback():
    root.after(DEF_PERIOD, timer_callback)
    # root.after(600, timer_callback)
    if root.timer_state == ST.WAITING or root.timer_state == ST.PAUSING or root.timer_state == ST.EXTEND:
        return
    root.remainder -= 1
    txt_timer.set(root.remainder)
    if root.remainder == 0:
        root.focus_force()
        if SOUND_FILE:
            playsound(SOUND_FILE)

        if root.timer_state == ST.TIMER:
            switch_state(ST.EXTEND)
        elif root.timer_state == ST.EXT_TIMER:
            btncb_stop()


# FIXME: timer has a one-minute error
# def timer_callback():
#     if root.timer_state == ST.WAITING or root.timer_state == ST.EXTEND:
#         return
#     if root.timer_state == ST.PAUSING:
#         root.after(DEF_PERIOD, timer_callback)
#         return
#
#     root.remainder -= 1
#     txt_timer.set(root.remainder)
#
#     if root.remainder != 0:
#         root.after(DEF_PERIOD, timer_callback)
#     else:
#         root.focus_force()
#         if SOUND_FILE:
#             simpleaudio.WaveObject.from_wave_file(SOUND_FILE).play()
#         if root.timer_state == ST.TIMER:
#             switch_state(ST.EXTEND)
#         elif root.timer_state == ST.EXT_TIMER:
#             btncb_stop()

if __name__ == '__main__':
    root.bind('<Key>', key_callback)
    switch_state(ST.WAITING)
    timer_callback()
    root.mainloop()
    log_stop()
